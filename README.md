# Socket Backend Piping

Socket based backend piping for a hierarchical communication platform with multiple user levels.

## Features

- Utilizes the socket.io's messaging system with added restrictions based on permission levels.
- Features requests with full length acknowledgments for reliability.
- Supports separate user ID other than socket ID, which helps in a single user-multi device scenario.
- Gives user disconnection notification to room head.
- Gives room end triggers for room head.
- Has room head disconnection action trigger with the ability to notify other moderators to accept room head privilege, if no one has accepted room head privilege, the room will be disconnected in preset time given in configuration files.
- Room status will be stored and users can access when they join.
- User log is stored.


## User levels

1. Admin
1. Room head
1. Moderator
1. Participant
1. Device


## Rooms

1. Admin
1. Moderator
1. Participant
1. Device
1. Conference

- Moderator first joins moderator namespace will be room head.
- Admin will be a member of moderator room.
- Everyone will be member in conference room.


## Communication paths

1. Devices cant make any requests.
2. All individuals can request server for last status.
3. Participant can make request to moderator.
4. Moderator can make request to any individual and a full length acknowledgment will be passed to every moderator.
5. Moderator can make broadcasts with partial acknowledgment from server.
6. Room head receives notification from admin and other moderators requesting screen.
7. Server can notify room head about user disconnection.
8. Server will notify other moderators on room head disconnection.
9. Server store broadcasts to provide last status.


## Variables

- `request`: Request identifier
- `payload`: Payload to process request
- `ackStat`: Acknowledgment request -> (true, false)
- `userId`: Unique user ID
- `userName`: User full name
- `connectionStat`: User connection status -> (connected, disconnected)
- `userRole`: User role -> (participant, moderator, admin, room head, device)
- `meetingStat`: Meeting Status -> (not started, class mode, discussion mode, suspended, disconnected )
- `requestType`: Request type -> (server, broadcast, individual, roomhead)


## Server Objects

### Room data
```
roomData = {
     roomID0: {roomHead:'', presenterId:'', presenterSocketId:'', startTime:'', updateTime:'', meetingStat:''},
     roomID1: {...}
}
```     

### User data
```
userData = {
     roomID0: {
          socketId_1: {socketId: '', userName:'', userId:'', userRole: '', connectionStat: ''},
          socketId_2: {...}
     },
     roomID1: {...}
}
```

## Incoming messages to moderator

### Device
// none

### Participant
- Hand rise
`{request:'hand rise', payload:{userID:'', socketId:''}, ackStat:false}`

### Moderator
- Presenter request
`{request:'presenter', payload:{userID:'', socketId:''}, ackStat:true}`

### Server
- Initial data
`{request:'initial data', payload:{userData:userData[roomId], roomData:roomData[roomId]}, ackStat:false}`

where,
```
userData[roomId] = {
     socketId_1: {socketId: '', userName:'', userId:'', userRole: '', connectionStat: ''},
     socketId_2: {...}
}
```
```
roomData[roomId] = {roomHead:'', presenterId:'', presenterSocketId:'', startTime:'', updateTime:'', meetingStat:''}
```
- User connection notification
`{request:'user join', payload:{userData:userData[roomId][socketId]}, ackStat:false}`

where,
```
userData[roomId][socketId] = {socketId: '', userName:'', userId:'', userRole: '', connectionStat: ''}
```


## Outgoing messages from moderator

### Genric format
`{request:'', payload:'', ackStat:'', requestType:''}`

#### Room updation
`{request:'', payload:{roomDetails:'', action:'update room'}, ackStat:true, requestType:'server'}`

#### Room head request
`{request:'', payload:'', ackStat:true, requestType:'roomhead'} // for request and payload refer moderator incoming message format`

#### Individual
`{request:'', payload:{socketId:'', data:''}, ackStat:'', requestType:'individual'}`

#### Broadcast
`{request:'', payload:'', ackStat:'', requestType:'broadcast'}`


## Incoming messages to participant

- Room data (both at joining and updation)

`roomData[roomId] = {roomHead:'', presenterId:'', presenterSocketId:'', startTime:'', updateTime:'', meetingStat:''}`


## Outgoing messages to moderator

- Participant request

`data = {request:'hand rise'}`
