module.exports = {
  env: {
    node: true,
    es2021: true,
  },
  extends: [
    'plugin:prettier/recommended',
    'airbnb-base',
  ],
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  plugins: [
    'prettier',
  ],
  rules: {
    'prettier/prettier': 'error',
    'no-unused-expressions': 'warn',
  },
};
