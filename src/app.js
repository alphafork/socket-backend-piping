const connection = require('./socket.js');
const admin = require('./admin.js');
const moderator = require('./moderator.js');
const participant = require('./participant.js');
const device = require('./device.js');

connection.adminNamespace.on("connection", (socket) => {
  admin.socketHandler(socket);
});

connection.moderatorNamespace.on("connection", (socket) => {
  moderator.socketHandler(socket);
});

connection.participantNamespace.on("connection", (socket) => {
  participant.socketHandler(socket);
});

connection.deviceNamespace.on("connection", (socket) => {
  device.socketHandler(socket);
});
