const events = require('./commonEventHandlers.js');

function socketHandler(socket){
  events.connectionEventHandler(socket, 'connected');
  events.messageRoomHead("device connected", socket, "", true);
  socket.emit("success"); // need to be updated with moderator acknowledgment
  socket.on("disconnect", () => {
    events.connectionEventHandler(socket, 'disconnected');
    events.messageRoomHead("device disconnected", socket, "", false);
  });
};

module.exports = {
  socketHandler
};
