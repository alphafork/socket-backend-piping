const connection = require('./socket.js');
const users = require('./manageUser.js');

let roomData = {}; // object with room details

const updateRoomData = function updateRoomDataAndNotifyEveryone(roomDetails, roomId){

  roomData[roomId] = {...roomData[roomId], ...roomDetails};
  connection.io.in(roomId).emit('update room data', roomData[roomId]);

  /* roomData = {
   *   room_no_1 : {roomHead:'', presenterId:'', presenterSocketId:'', startTime:'', updateTime:'', meetingStat:''}
   * }; */
  // update roomData with values passes as roomDetails object
  // notify everyone
};

const createRoom = function createRoomAddUsersAddRoomHeadNotifyModerators(socket){

  const roomId = socket.handshake.query.roomId;
  if (!(roomId in roomData)){
    const startTime = UpdateTime = Date.now();
    const newRoomData = {startTime:startTime, updateTime:startTime, meetingStat:'active'}
    updateRoomData(newRoomData, roomId);
    createRoomHead(socket);
    socket.join(roomId);

    for (key in users.userData[roomId]){
      const userSocket = connection.participantNamespace.sockets.get(key);
      if (userSocket) {
        userSocket.join(roomId);
      };
    };
  };
  console.log(roomData);
  const message = {request:'initial data', payload:{userData:users.userData[roomId], roomData:roomData[roomId]}, ackStat:false};
  socket.emit('managed request', message);
  // if no room
  // create room
  // join all users with roomId in user object to that room
  // set room head
  // pass data to room head
};

const createRoomHead = function createRoomHeadAndInformClassroom(socket){
  const socketId = socket.id;
  const userId = socket.handshake.query.userId;
  const roomId = socket.handshake.query.roomId;
  const roomHeadData = {roomHeadId:userId, roomHeadSid:socketId, presenterSid:socketId, presenterId:userId}
  updateRoomData(roomHeadData, roomId);
  // update room head by updating roomData object with room head's values
};

const clearRoomHead = function checkAndClearRoomHead(socket){
  const roomId = socket.handshake.query.roomId;
  if (roomData[roomId]['roomHeadSid'] == socket.id){
    const updateTime = Date.now();
    const roomHeadData = {roomHeadId:'', roomHeadSid:'', presenterSid:'', presenterId:'', updateTime:updateTime, meetingStat:'suspended'};
    updateRoomData(roomHeadData, roomId);
  };
  // clear if room head
};

module.exports = {
  roomData,
  createRoom,
  clearRoomHead
};
