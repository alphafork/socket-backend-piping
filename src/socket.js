const config = require('./config.js')
const { createServer } = require("http");
const httpServer = createServer();
const { Server } = require("socket.io");
const options = {
  transports: ['websocket']
};
const io = new Server(httpServer, options);

console.log("server started");

const adminNamespace = io.of("/admin");
const moderatorNamespace = io.of("/moderator");
const participantNamespace = io.of("/participant");
const deviceNamespace = io.of("/device");

httpServer.listen(config.port);

module.exports = {
  io,
  adminNamespace,
  moderatorNamespace,
  participantNamespace,
  deviceNamespace
};
