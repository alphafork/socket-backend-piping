const events = require('./commonEventHandlers.js');
const rooms = require('./manageRoom.js');
const moderator = require('./moderator.js');

function socketHandler(socket){
  events.connectionEventHandler(socket, 'connected');
  rooms.createRoom(socket);
  socket.on('client event', (data) => {
    moderator.moderatorEvent(socket, data);
  });
  socket.on("disconnect", () => {
    events.connectionEventHandler(socket, 'disconnected');
    rooms.clearRoomHead(socket);
  });
};

module.exports = {
  socketHandler
};
