const users = require('./manageUser.js');
const rooms = require('./manageRoom.js');

const connectionEventHandler = function connectionStateChangeHander(socket, connectionStat){
  const roomId = socket.handshake.query.roomId;
  if (!(roomId in users.userData)){
    users.userData[roomId] = {};
  };
  const socketId = socket.id;
  const userName =  socket.handshake.query.userName;
  const userId =  socket.handshake.query.userId;
  const userRole =  socket.handshake.query.userRole;
  const payload = {socketId:socketId, userName:userName, userId:userId, userRole:userRole, connectionStat:connectionStat};
  if (connectionStat == "connected"){
    users.userData[roomId][socketId] = {};
  };
  users.updateUserData(socketId, roomId, payload);
};

const messageRoomHead = function messageRoomHead(request, socket, payload, ackStat){
  const roomId = socket.handshake.query.roomId;
  const roomHeadId = rooms.roomData.roomId.roomHeadId;
  const message = {request:request, payload:payload, ackStat:ackStat};
  socket.to(roomHeadId).emit("managed request", message);
  // socket emit to room head
};

/* const deviceEvent = deviceSocketEvent(socket. data){
 *   // handle for future
 * };
 *
 * const acknowledgmentManagement = moderatorAcknowledgmentManagement(data){
 *   // server acknowledgment
 *   // full length acknowledgment
 * }; */

module.exports = {
  connectionEventHandler,
  messageRoomHead
};
