const connection = require('./socket.js');

let userData = {}; // object with user details

const updateUserData = function updateUserDataAndNotifyModerator(socketId, roomId, payload){
  userData[roomId][socketId] = {...userData[roomId][socketId], ...payload}
  console.log(userData);
  const message = {request:'user update', payload:payload, ackStat:false};
  connection.moderatorNamespace.emit('managed request', message);

  /* socket.handshake.query = { userId: "user id", userName: "user name", userRole: "moderator", roomId: "room id", EIO: "4", transport: "websocket" }
   * userData = {
   *   room_no_1: {
   *     socketId_1: {socketId: socketId_1, userName:userName_1, userId:userId_1, userRole: "moderator", connectionStat: "connected"},
   *     socketId_2: {socketId: socketId_2, userName:userName_2, userId:userId_2, userRole: "student", connectionStat: "connected"}
   *   },
   *   room_no_2: {
   *     socketId_3: {socketId: socketId_3, userName:userName_3, userId:userId_3, userRole: "moderator", connectionStat: "connected"},
   *     socketId_4: {socketId: socketId_4, userName:userName_4, userId:userId_4, userRole: "student", connectionStat: "connected"}
   *   }
   * } */
  // update userData object when a new user is joined
  // notify moderator
};


module.exports = {
  updateUserData,
  userData
};
