const events = require('./commonEventHandlers.js');
const rooms = require('./manageRoom.js');

function socketHandler(socket){
  events.connectionEventHandler(socket, 'connected');
  rooms.createRoom(socket);
  socket.on('client event', (data) => {
    moderatorEvent(socket, data);
  });
  socket.on("disconnect", (reason) => {
    console.log(reason);
    events.connectionEventHandler(socket, 'disconnected');
    rooms.clearRoomHead(socket);
    /* updateRoomHead(); */
  });
};

const moderatorEvent = function moderatorSocketEvent(socket, data){
  const request = data.request;
  const payload = data.payload;
  const requestType = data.requestType;

  if (requestType == 'server'){
    if (request == 'update room'){
      updateRoomData(payload, socket.handshake.query.roomId);
    } else if (request == 'update user'){
      const roomId = socket.handshake.query.roomId;
      const socketId = socket.id;
      users.updateUserData(socketId, roomId, payload);
    };
  } else if (requestType == 'individual'){
    const message = {request:request, payload:payload.data, ackStat:true};
    const target = payload.socketId;
    socket.to(target).emit('moderator request', message);
  } else if (requestType == 'roomhead'){
    events.messageRoomHead(request, socket, payload, true);
  } else if (requestType == 'broadcast'){
    const roomId = socket.handshake.query.roomId;
    const message = {request:request, payload:payload, ackStat:false};
    socket.to(roomId).emit('broadcast', message);
  };
  //  const message = {request, payload, ackStat};
  // broadcasts
  // requests to individual participants
  // broadcasts to rooms
  // requests to room head
 // acknowledgentManagement(data);
};

module.exports = {
  socketHandler,
  moderatorEvent
};
