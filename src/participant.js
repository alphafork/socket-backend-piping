const events = require('./commonEventHandlers.js');
const users = require('./manageUser.js');

function socketHandler(socket){
  events.connectionEventHandler(socket, 'connected');
  socket.on('client request', (data) => {
    participantEvent(socket, data)
  });
  socket.on("disconnect", () => {
    events.connectionEventHandler(socket, 'disconnected');
  });
};

const participantEvent = function participantSocketEvent(socket, data){
  const request = data.request;
  const payload = data.payload;
  const requestType = data.requestType;
  if (requestType == 'server'){
    if (request == 'update user'){
      const roomId = socket.handshake.query.roomId;
      const socketId = socket.id;
      users.updateUserData(socketId, roomId, payload);
    };
  } else if (requestType == 'roomhead'){
    events.messageRoomHead(request, socket, socket.id, false);
    // requests to room head
  };
};

module.exports = {
  socketHandler,
  participantEvent
};
